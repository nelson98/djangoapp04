from django.urls import path
from . import views

app_name = 'votacion'

urlpatterns = [
    #ex: /votacion/
    path('', views.index, name='index'),
    #ex: /votacion/5/
    path('<int:region_id>/', views.detalle, name='detalle'),
    #ex: /votacion/5/resultados/
    path('<int:region_id>/resultados/', views.resultados, name='resultados'),
    #ex: /votacion/5/voto/
    path('<int:region_id>/voto/', views.votar, name='votar'),
]